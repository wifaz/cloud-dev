# cloud-dev

## About

Automatic setup of cloud server for development and remote pairing in the cloud. 
Starting point is a standard debian stable install (currently stretch)

Currently only one Cloud provider is supported, namely Hetzner. Others may or may not follow.

A script is provided, that sets up a could server, and via cloud-init installs additional packages.
Then it opens an xpra cloud server on the server and an xpra client on the local machine.
Xpra lets you view X-Windows applications remotely. An xterm will be displayed, from which you can
run other applications. Their windows will also pop up on your local machine.

Xpra is complete Open Source and allows remote pairing.


## INSTALL

    # Currently only usable for Hetzner customers
    git clone https://gitlab.com/wifaz/cloud-dev.git cloud-dev
    cd cloud-dev
    cp config.py.sample config.py
    # Now edit config.py
    virtualenv -p python3 env
    pip install hcloud
    
## Use

    env/bin/python cloud_setup.py
    # Wait for xterm to pop up. This may take a while
