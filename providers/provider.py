class Provider:
    """Abstract base class for cloud server providers"""
    def create_server(self, user_data):
        """Create a cloud server. 

        Will be called only once. The provider instance is responsible for keeping the
        the necessary data to connect to the server.

        Parameters
        ----------
        user_data : string
                    cloud-init configuration 
                    (see https://cloudinit.readthedocs.io/en/latest/topics/examples.html )
        """
        raise NotImplementedError()

    def get_ip(self):
        """Return the ip address of the server a string"""
        raise NotImplementedError()

    def get_apt_mirror(self):
        """Return debian archive mirror uri, if the provider has one inhouse."""
        return None
        
class ProviderException(Exception):
    pass
