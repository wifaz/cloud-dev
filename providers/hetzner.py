from hcloud import Client
from hcloud.hcloud import APIException
from hcloud.server_types.domain import ServerType
from hcloud.images.domain import Image
from time import sleep
from .provider import Provider, ProviderException

class Hetzner(Provider):
    """XXX config"""
    def create_server(self,user_data):
        self.client = Client(token="%s"%self.api_token)
        response = self.client.servers.create(
            name=self.server_name,
            server_type=ServerType(name=self.server_type),
            image=Image(name=self.image_name),
            ssh_keys=self._get_ssh_keys(),
            user_data=user_data,
        )
        server = response.server
        print("hetzner cloud server created")
        print("server name:"+server.name)
        print("server ip: "+server.public_net.ipv4.ip)
        self._wait_for_server(100)

    def _get_ssh_keys(self):
        ssh_key_name = getattr(self,"ssh_key_name",None)
        if not ssh_key_name:
            return []
        ssh_keys = self.client.ssh_keys.get_all(name=ssh_key_name)
        if len(ssh_keys)==0:
            raise ProviderException("Ssh key not found, please check config.provider.ssh_key_name")
        elif len(ssh_keys)>1:
            raise ProviderException("More than one ssh key found for name (This should not happen)")
        print("ssh key found: "+ssh_keys[0].name)
        return ssh_keys

    def _get_server(self):
        server = self.client.servers.get_by_name(self.server_name)
        return server

    def _wait_for_server(self,wait_max):
        for i in range(wait_max):
            server = self._get_server()
            status = server.status
            print("server status: "+status)
            if status=="running":
                return
            sleep(1)
        raise ProviderException("Server does not reach status 'running'")

    def get_ip(self):
        return self._get_server().public_net.ipv4.ip

    def get_apt_mirror(self):
        return "http://mirror.hetzner.de/debian/packages"


def _list_available_images(self,client):
    self.client = Client(token="%s"%self.api_token)
    images = client.images.get_all()
    print("Available images:")
    for im in images:
        print("    "+im.name)

    
