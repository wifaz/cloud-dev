import os
from os import system
from time import sleep
from config import provider
from config import files, command, username, user_keys, packages, xcommand

DATA_DIR = "cloud-data/"
HOSTKEYS_DIR = DATA_DIR+"hostkeys/"
USERDATA_FILE = DATA_DIR+"user-data.txt"
SCRIPT_DIR = os.path.dirname(os.path.abspath(__file__)) + "/"
RESOURCE_DIR = SCRIPT_DIR+"resources/"

KEY_TYPES = "rsa dsa ecdsa".split()


def get_user_data():
    return open(USERDATA_FILE).read()

def setup(host):
    if files:
        filesstr = " ".join(files)
        
        print("--- Copying files to server: %s ---" % filesstr)
        cmd = "scp -o ConnectTimeout=5 %s root@%s:" % (filesstr, host)
        retry_loop(cmd)

    if command:
        print("--- Executing setup command on server: %s ---" % command)    
        cmd = "ssh -o ConnectTimeout=5 root@%s %s" % (host,command)
        retry_loop(cmd)
        print("setup complete")

def retry_loop(cmd):
    n = 5
    for i in range(n):
        print("$ "+cmd)
        err = system(cmd)
        if err:
            if i>=n-1:
                print("Fatal error: giving up after trying %d times."%n)
                exit(1)
            else:
                print("command failed, retrying in 5 seconds...")
                sleep(5)
        else:
            break    
    
def create_host_keys():
    dir = DATA_DIR+"hostkeys"
    os.makedirs(HOSTKEYS_DIR, exist_ok=True)
    keys = os.listdir(HOSTKEYS_DIR)
    if not keys:
        print("--- Gererating server keys ---")
        for type in KEY_TYPES:
            cmd = "cd %(dir)s; ssh-keygen -t %(type)s -N '' -f ssh_host_%(type)s_key" % {"dir": HOSTKEYS_DIR, "type": type}
            print("$", cmd)
            system(cmd)

def create_user_data(provider):
    os.makedirs(DATA_DIR, exist_ok=True)
    with open(USERDATA_FILE,"w") as ostream:
        ostream.write("#cloud-config\n\n")
        write_user(ostream)
        write_host_keys(ostream)
        write_apt_conf(ostream,provider)
        write_packages(ostream)
        write_run_xpra(ostream)
    
def write_host_keys(ostream):
    create_host_keys()
    ostream.write("ssh_deletekeys: true\n")
    ostream.write("disable_root: true\n")
    ostream.write("ssh_keys:\n")
    for type in KEY_TYPES:
        ostream.write("  %s_private: |\n" % type)
        filename = HOSTKEYS_DIR+"ssh_host_%s_key" % type
        write_file_indented(filename, ostream, 4)
        ostream.write("  %s_public: " % type)
        filename = HOSTKEYS_DIR+"ssh_host_%s_key.pub" % type
        with open(filename) as istream:
            ostream.write(istream.read())
        ostream.write("\n")

def write_file_indented(filename, ostream, n):
    with open(filename) as inp:
        for line in inp.readlines():
            ostream.write(" "*n+line)
    ostream.write("\n")

def write_user(ostream):
    s = """users:
  - name: %s
    gecos: Developer
    shell: /bin/bash
    sudo: ALL=(ALL) NOPASSWD:ALL
    ssh_authorized_keys:
""" % username
    ostream.write(s)
    for filename in user_keys:
        with open(filename) as istream:
            ostream.write( "      - "+istream.read())
    ostream.write("\n")

def write_apt_conf(ostream,provider): 
    s = """
# Disable apt package caching, it only wastes disc space
write_files:
  - content: |
        Dir::Cache "";
        Dir::Cache::archives "";
    path: /etc/apt/apt.conf.d/00_disable-cache-directories

apt:
  primary:
    - arches: [default]
      search:"""
    ostream.write(s)
    mirror = provider.get_apt_mirror()
    if mirror:
        ostream.write("\n        - "+mirror)
    s = """
        - http://deb.debian.org/debian/
  security:
    - arches:
        - default
  sources:
    xpra:
      source: "deb http://winswitch.org/beta/ stretch main"
      source: "deb http://winswitch.org/ stretch main"
      key: |
"""
    ostream.write(s)
    write_file_indented(RESOURCE_DIR+"winswitch-key.asc", ostream, 8)
    s = """
  sources_list: |
        deb $MIRROR $RELEASE main

"""    
    ostream.write(s)

def write_packages(ostream):
    # xpra minimal packages
    s = """packages:
  - xserver-xorg-core
  - xauth
  - python-paramiko
  - xterm
  - xpra
"""
    ostream.write(s)

    # user packages
    for p in packages:
        ostream.write("  - "+p+"\n")
    ostream.write("\n")

def write_run_xpra(ostream):
    s = """
write_files:
  - content: |
        #!/bin/bash
        mkdir -p /run/user/`id -u`/xpra/
        Xorg -noreset +extension GLX +extension RANDR +extension RENDER -logfile /run/user/`id -u`/xpra/xorg:30 -config /etc/xpra/xorg.conf :30 &
        /usr/bin/xpra start --use-display :30 --start-child=%(xcommand)s --desktop-scaling=off --quality=100 --sharing=yes
    path: /usr/local/bin/start-xpra-server
    permissions: '0755'

runcmd:
  - 'while [ ! -f /usr/bin/xpra ] ; do echo wait; sleep 2; done'
  - 'sudo -u %(username)s -- /usr/local/bin/start-xpra-server'
  - 'touch /tmp/xpra-ready'

""" % {"xcommand": xcommand, "username": username}
    ostream.write(s)    
    
def start_xpra_client(host):
    print("--- Polling server... ---")
    cmd = "ssh -o ConnectTimeout=5 %s@%s test -f /tmp/xpra-ready"%(username, host)
    while True:
        print("$ "+cmd)
        if system(cmd)!=0:
            print("server not ready, waiting ...")
            sleep(2)
        else:
            break
    cmd = "xpra attach ssh://%s@%s/30  --desktop-scaling=off --quality=100"%(username, host)
    print("$ "+cmd)
    system(cmd)

def main():
    create_user_data(provider)
    server = provider.create_server(get_user_data())
    host = provider.get_ip()
    print("host ip is:", host)
    sleep(5)
    setup(host)
    start_xpra_client(host)
    
if __name__=="__main__":
    # create_user_data(provider)
    main()

